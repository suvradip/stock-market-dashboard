# BASIC STOCK MARKET DASHBOARD

> A simple dashboards which displays stock price trends

## Requirements

To run this application in your system you need

1. Node.js
2. Free ports 5481, 8080

## Installation

```bash
# install dependencies
yarn install

# start the rect app, serve with hot reload at localhost:5481, try to run this in same port
yarn run start:client

# start the server in debug mode with auto restart, please keep this port to 8080
yarn run debug

# build for production and launch server
yarn run build
yarn run start
```

## Required settings or Variable values

> I'm using https://www.alphavantage.co/ for data, please create an api key before running this application.

Create a `.env` file in the project root with all the variable values

```bash
PORT=8080
NODE_ENV=production

MONGODB_URL=<with username:password>
ALPHA_VANTAGE_API_BASEURL=
ALPHA_VANTAGE_API_KEY=
```

### Dashboard

![Simple dashboard](./snaps/s1.png)
![Simple dashboard](./snaps/s2.png)
