import axios from 'axios';

const isProd = process.env.NODE_ENV === 'production';
const port = process.env.PORT || '8080';

const API_ROOT = isProd ? '/api/v1' : `http://localhost:${port}/api/v1`;

const instance = axios.create({
   baseURL: API_ROOT,
});

const responseBody = (res) => res.data;

/* for the stock market data */
const stock = (symbol, duration) => instance.get(`/stock`, { params: { symbol, duration } }).then(responseBody);

/* searching */
const search = (q) => instance.get(`/search/`, { params: { q } }).then(responseBody);

export default {
   search,
   stock,
};
