import { SEARCH_COMPANY, COMPANY_SELECTED } from '../actionTypes';

export default (
   state = {
      result: [],
      selected: { symbol: 'AAPL', name: 'Apple Inc.' },
      frequency: 'monthly',
   },
   action
) => {
   switch (action.type) {
      case SEARCH_COMPANY:
         return {
            ...state,
            result: action.payload,
         };

      case COMPANY_SELECTED: {
         return {
            ...state,
            selected: action.payload,
         };
      }

      default:
         return state;
   }
};
