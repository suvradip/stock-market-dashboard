/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */

import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
// import DropDown from './ResultDropDown/ResultDropDown';
import api from '../../util/api';

import './SearchBar.scss';

const SearchBar = (props) => {
   const [result, setResult] = useState([]);
   const [showListFlag, setShowListFlag] = useState(false);
   const { selectCompany } = props;

   const debounce = () => {
      let id;
      return (event) => {
         const targetValue = event.target.value;
         clearTimeout(id);
         id = setTimeout(() => {
            if (targetValue === '') {
               setResult([]);
            } else {
               const promises = [api.search(targetValue)];
               Promise.all(promises).then(([v]) => {
                  setResult(v.data);
                  // fetchResult(v.data);
               });
            }
         }, 1000);
      };
   };

   const handleChange = debounce();

   const selectAction = ({ symbol, name }) => {
      selectCompany({ symbol, name });
      setShowListFlag(false);
   };

   const node = useRef();

   const handleClick = (e) => {
      if (!node.current.contains(e.target)) {
         setShowListFlag(false);
      }
   };

   useEffect(() => {
      document.addEventListener('mousedown', handleClick);

      return () => {
         document.removeEventListener('mousedown', handleClick);
      };
   }, []);

   const updateState = () => {
      setShowListFlag(true);
   };

   return (
      <div className='search-bar'>
         <div className='input-group '>
            <input
               type='text'
               className='form-control'
               placeholder='Search ...'
               onClick={updateState}
               onChange={handleChange}
            />
         </div>

         <div className=' result' ref={node}>
            {result.length > 0 && showListFlag && (
               <div className='menu' aria-labelledby='dropdownMenuLink'>
                  {result.map((rs) => (
                     <div className='item' key={rs.symbol} onClick={() => selectAction(rs)}>
                        <span className='symbol'>{rs.symbol}</span> <span>{rs.name}</span>
                     </div>
                  ))}
               </div>
            )}
         </div>
      </div>
   );
};

SearchBar.propTypes = {
   selectCompany: PropTypes.func.isRequired,
};

export default SearchBar;
