/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import './App.scss';
import { connect } from 'react-redux';
import Navbar from '../Navbar/Navbar';
import SearchBar from '../SearchBar/SearchBar';
import Chart from '../Chart/Chart';
import { SEARCH_COMPANY, COMPANY_SELECTED } from '../../store/actionTypes';

const mapStateToProps = (state) => ({
   ...state.search,
});

const mapDispatchToProps = (dispatch) => ({
   fetchSearchResult: (payload) => dispatch({ type: SEARCH_COMPANY, payload }),
   companySelected: (payload) => dispatch({ type: COMPANY_SELECTED, payload }),
});

class App extends Component {
   constructor(props) {
      super(props);
      this.state = {
         frequency: 'monthly',
      };
   }

   handleFrequencyChange = (evt) => {
      const { value } = evt.target;
      this.setState({ frequency: value });
   };

   render() {
      const { frequency } = this.state;
      const { fetchSearchResult, companySelected } = this.props;
      return (
         <div className='container-fluid'>
            <Navbar />
            <div className='container'>
               <div className='row mt-5'>
                  <div className='col-8'>
                     <SearchBar fetchResult={fetchSearchResult} selectCompany={companySelected} />
                  </div>
                  <div className='col-4'>
                     <div className='widgets'>
                        <h6 className='d-inline-block mr-4'>Frequency</h6>
                        <select value={frequency} onChange={this.handleFrequencyChange}>
                           <option value='daily'>Daily</option>
                           <option value='weekly'>Weekly</option>
                           <option value='monthly'>Monthly</option>
                        </select>
                     </div>
                  </div>
               </div>

               <div className='row'>
                  <div className='col-12'>
                     <div className='stock-chart text-center'>
                        <Chart duration={frequency} />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      );
   }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
