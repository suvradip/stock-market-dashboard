import React from 'react';
import logo from '../../images/logo.svg';
import './Navbar.scss';

const Navbar = () => {
   return (
      <nav className='navbar navbar-light bg-light'>
         <a href='/'>
            <img src={logo} width='auto' height='60px' alt='demo logo' />
         </a>
      </nav>
   );
};

export default Navbar;
