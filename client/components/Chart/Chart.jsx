import React, { Component } from 'react';
import FusionCharts from 'fusioncharts';
import TimeSeries from 'fusioncharts/fusioncharts.timeseries';
import ReactFC from 'react-fusioncharts';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import api from '../../util/api';

ReactFC.fcRoot(FusionCharts, TimeSeries);

const mapStateToProps = (state) => ({
   ...state.search,
});

class Chart extends Component {
   constructor(props) {
      super(props);
      this.state = {
         timeseriesDs: {
            type: 'timeseries',
            width: '100%',
            height: '600',
            dataSource: {
               chart: {},
               caption: {},
               yAxis: [
                  {
                     plot: {
                        value: {
                           open: 'open',
                           high: 'high',
                           low: 'low',
                           close: 'close',
                        },
                        type: 'candlestick',
                     },
                     format: {
                        prefix: '$',
                     },
                     title: 'Stock Value',
                  },
                  {
                     plot: [
                        {
                           value: 'volume',
                           type: 'column',
                        },
                     ],
                     // max: '900000000',
                  },
               ],
            },
         },
      };

      this.createDataTable = this.createDataTable.bind(this);
   }

   componentDidMount() {
      this.createDataTable();
   }

   componentDidUpdate(prevProps) {
      const { duration, selected } = this.props;
      if (duration !== prevProps.duration || selected !== prevProps.selected) {
         this.createDataTable();
      }
   }

   createDataTable() {
      const { timeseriesDs } = this.state;
      const { duration, selected } = this.props;

      Promise.all([api.stock(selected.symbol, duration)]).then(([res]) => {
         const { result, schema } = res.data;
         const parseSchema = schema.map((name, i) => {
            if (i === 0) {
               return { name, type: 'date', format: '%Y-%m-%d' };
            }
            return {
               name,
               type: 'number',
            };
         });

         const fusionDataStore = new FusionCharts.DataStore();
         const fusionTable = fusionDataStore.createDataTable(result, parseSchema);
         const tsData = timeseriesDs;
         tsData.dataSource.data = fusionTable;
         tsData.dataSource.caption.text = selected.name;
         this.setState({
            timeseriesDs: tsData,
         });
      });
   }

   render() {
      const { timeseriesDs } = this.state;
      return (
         <div className='App'>
            {/* eslint-disable-next-line */}
            <ReactFC {...timeseriesDs} />
         </div>
      );
   }
}

Chart.propTypes = {
   duration: PropTypes.string.isRequired,
   selected: PropTypes.object.isRequired,
};

// export default Chart;
export default connect(mapStateToProps, () => ({}))(Chart);
