const supertest = require('supertest');
const { expect } = require('chai');
const winston = require('winston');
const { app, server } = require('../server');
const logger = require('../server/util/logger');

/* removing console logger while doing testing */
logger.remove(winston.transports.Console);
const request = supertest(app);

before(async function () {
   this.timeout(1000 * 30);
   /* waiting for 5 SEC before starting test test cases  */
   await new Promise((resolve) => setTimeout(resolve, 1000 * 5));
});

describe('SERVER APIs', () => {
   it('GET /api', (done) => {
      request.get('/api').set('Accept', 'application/json').expect('Content-Type', /json/).expect(200, done);
   });

   it('GET /api/v1/search', async () => {
      request
         .get('/api/v1/search?q=ibm')
         .set('Accept', 'application/json')
         .expect('Content-Type', /json/)
         .expect(200)
         .then((resp) => {
            const jsonResp = resp.body;
            expect(jsonResp.keywords).to.be.equal('ibm');
            expect(jsonResp.data.length).to.be.greaterThan(1);
         });
   });
});

after(function (done) {
   server.close(() => {
      done();
      process.exit(0);
   });
});
