const axios = require('../config/axios');
const { updateKeyValue } = require('../util/dataModifier');
const { Keywords } = require('../models');
const mainLogger = require('../util/logger');

const logger = mainLogger.child({ reporter: __filename });

const KEY = process.env.ALPHA_VANTAGE_API_KEY;

class Search {
   constructor() {
      this.dataType = 'json';
   }

   async get(keywords) {
      logger.debug(`searching get method invoked`);
      this.keywords = keywords;
      this.data = await this.findDB();
      if (this.data) return this.data;

      this.data = await Search.fetchOrigin(keywords, this.dataType);
      Search.updateDB(keywords, this.data);
      return this.data;
   }

   async findDB() {
      logger.debug(`searching with keywords ${this.keywords}`);
      return Keywords.find({ keywords: this.keywords })
         .lean()
         .then((data) => (typeof data[0] !== 'undefined' ? data[0].result : false));
   }

   static updateDB(keywords, result) {
      logger.debug(`saving keywords search result data to db with keywords - ${this.keywords}`);
      Keywords.findOneOrCreate({ keywords }, { keywords, result });
   }

   static fetchOrigin(keywords, format) {
      logger.debug(`fetching from origin server with keywords - ${this.keywords}`);
      return axios
         .get('/', {
            params: {
               function: 'SYMBOL_SEARCH',
               datatype: format,
               keywords,
               apikey: KEY,
            },
         })
         .then((res) => res.data.bestMatches)
         .then((data) => updateKeyValue(data));
   }
}

module.exports = Search;
