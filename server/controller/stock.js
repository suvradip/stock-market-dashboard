const { isToday, compareDesc } = require('date-fns');
const axios = require('../config/axios');
const { Stock } = require('../models');
const mainLogger = require('../util/logger');
const { updateKeyValue } = require('../util/dataModifier');

const logger = mainLogger.child({ reporter: __filename });

const KEY = process.env.ALPHA_VANTAGE_API_KEY;

function dataParser(obj, lastEntry) {
   logger.debug('Data parsing started');
   const [meta, data] = Object.keys(obj);
   if (typeof data === 'undefined') return false;

   const metaData = updateKeyValue([obj[meta]])[0];
   const result = obj[data];
   let flag = true;
   let definition = ['date'];
   const parsedData = [];

   const keys = Object.keys(result);
   for (let i = 0; i < keys.length; i += 1) {
      const ts = keys[i];
      const arr = [ts];
      const item = result[ts];
      const dataDate = new Date(ts);

      if (compareDesc(lastEntry, dataDate) === 1) {
         if (flag) {
            definition = definition.concat(Object.keys(updateKeyValue([item])[0]));
            flag = false;
         }
         parsedData.push(arr.concat(Object.values(item).map((a) => Number(a))));
      } else {
         break;
      }
   }

   logger.debug(`dataParsing complete. Data size: ${parsedData.length}`);
   return { data: parsedData, meta: metaData, definition };
}

class StockCtrl {
   constructor({ duration, symbol }) {
      this.duration = duration;
      this.symbol = symbol;
   }

   async get() {
      const lastEntry = await this.find();
      this.lastEntry = new Date(lastEntry && lastEntry.updatedAt);
      let result;
      if (isToday(this.lastEntry)) {
         result = await this.getDBData();
         logger.debug("Old data found by today's date");
         return result;
      }

      logger.debug('fetching new data via api');
      this.data = await this.fetchOrigin();
      if (!this.data) return false;
      result = await this.storeDb();
      logger.debug('Date prepared');
      return result;
   }

   fetchOrigin() {
      logger.debug(`Symbol - ${this.symbol} Duration - ${this.duration}`);
      return axios
         .get('/', {
            params: {
               function: this.duration,
               symbol: this.symbol,
               datatype: 'json',
               apikey: KEY,
               //  outputsize: 'full',
            },
         })
         .then((res) => res.data)
         .then((data) => dataParser(data, this.lastEntry));
   }

   async storeDb() {
      logger.debug('DB data update start');
      await Stock.updateOne(
         { symbol: this.symbol, frequency: this.duration },
         {
            $push: { data: { $each: this.data.data, $position: 0 } },
         }
      );

      logger.debug('DB data update complete');

      return Stock.findOneOrCreate(
         { symbol: this.symbol, frequency: this.duration },
         {
            symbol: this.symbol,
            data: this.data.data,
            dataDefinition: this.data.definition,
            timeZone: this.data.meta.Time,
            frequency: this.duration,
         }
      );
   }

   getDBData() {
      return Stock.findOne({ symbol: this.symbol, frequency: this.duration }).lean();
   }

   find() {
      return Stock.findOne({ symbol: this.symbol, frequency: this.duration }, { updatedAt: true }).lean();
   }
}

module.exports = StockCtrl;
