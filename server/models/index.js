const Stock = require('./Stock.model');
const Keywords = require('./Keywords.model');

module.exports = {
   Stock,
   Keywords,
};
