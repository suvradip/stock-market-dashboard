const mongoose = require('mongoose');

const { Schema } = mongoose;

const schema = new Schema(
   {
      id: { type: Schema.ObjectId },
      symbol: { type: String, required: true, index: true },
      frequency: { type: String, required: true },
      data: { type: Schema.Types.Mixed, default: [] },
      timeZone: { type: String },
      dataDefinition: { type: Array, default: [] },
   },
   {
      timestamps: true,
   }
);

schema.statics.findOneOrCreate = function findOneOrCreate(condition, doc) {
   const self = this;
   const newDocument = doc;
   return new Promise((resolve, reject) => {
      return self
         .findOne(condition)
         .then((result) => {
            if (result) {
               return resolve(result);
            }

            return self
               .create(newDocument)
               .then((data) => resolve(data))
               .catch((error) => reject(error));
         })
         .catch((error) => reject(error));
   });
};

module.exports = mongoose.model('Stocks', schema);
