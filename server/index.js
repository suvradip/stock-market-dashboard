require('dotenv').config();
const express = require('express');
const consola = require('consola');
const bodyParser = require('body-parser');
const logger = require('morgan');
const path = require('path');

const apiRouter = require('./api');
require('./config/mongodb')();

const app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, '..', 'build')));

app.use('/api', apiRouter);

const PORT = process.env.PORT || 8080;

const server = app.listen(PORT);
consola.ready({
   message: `Server listening on http://localhost:${PORT}`,
   badge: true,
});

module.exports = { server, app };
