const axios = require('axios');

const API_ROOT = process.env.ALPHA_VANTAGE_API_BASEURL || 'https://www.alphavantage.co/query';

const instance = axios.create({
   baseURL: API_ROOT,
});

module.exports = instance;
