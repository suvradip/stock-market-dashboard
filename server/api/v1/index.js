const consola = require('consola');
const router = require('express').Router();
const Search = require('../../controller/search');
const StockCtrl = require('../../controller/stock');
const { getFrequencyName } = require('../../util/dataModifier');
const mainLogger = require('../../util/logger');

const logger = mainLogger.child({ reporter: __filename });

const searchInstance = new Search();

function processError(error, res) {
   consola.error(error);
   return res.status(500).json({
      error: 'Server Error!',
   });
}

router.get('/search', async (req, res) => {
   logger.debug(`hit /api/v1/search ${JSON.stringify(req.query)}`);

   try {
      const { q } = req.query;
      if (typeof q === 'undefined' && q === '') return res.status(400).json({ error: 'Query is not present.' });

      const result = await searchInstance.get(q);
      return res.json({
         message: 'OK',
         data: result,
         keywords: q,
      });
   } catch (error) {
      return processError(error, res);
   }
});

router.get('/stock', async (req, res) => {
   logger.debug(`hit /api/v1/stock ${JSON.stringify(req.query)}`);

   try {
      const { duration, symbol } = req.query;
      const durationLabel = getFrequencyName(duration);
      const instance = new StockCtrl({ symbol, duration: durationLabel });
      const resp = await instance.get();
      return res.send({
         message: 'OK',
         data: {
            schema: resp.dataDefinition,
            symbol: resp.symbol,
            result: resp.data,
         },
      });
   } catch (error) {
      return processError(error, res);
   }
});

module.exports = router;
