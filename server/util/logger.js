const winston = require('winston');
const path = require('path');

const formatMessage = (inf) => {
   const module = (inf.reporter && inf.reporter.replace(/[\s\S]+server\//gm, '')) || '';
   return `${inf.timestamp}::[${inf.level}]::[${module}]::${inf.message}`;
};

const logger = winston.createLogger({
   level: 'debug',
   format: winston.format.combine(
      winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss' }),
      winston.format.printf((inf) => formatMessage(inf))
   ),
   transports: [
      new winston.transports.Console(),
      new winston.transports.File({ filename: path.resolve(__dirname, '../../logs/app.log') }),
   ],
});

module.exports = logger;
