function updateKeyValue(arr) {
   const freshArr = [];
   for (let i = 0; i < arr.length; i += 1) {
      const item = arr[i];
      const obj = {};
      Object.keys(item).forEach((a) => {
         const [, key] = a.split(' ');
         obj[key] = item[a];
      });
      freshArr.push(obj);
   }

   return freshArr;
}

function getFrequencyName(key) {
   switch (key) {
      case 'daily':
         return 'TIME_SERIES_DAILY';
      case 'weekly':
         return 'TIME_SERIES_WEEKLY';
      case 'monthly':
         return 'TIME_SERIES_MONTHLY';
      default:
         //  return 'TIME_SERIES_INTRADAY ';
         return 'TIME_SERIES_DAILY';
   }
}
module.exports = { updateKeyValue, getFrequencyName };
